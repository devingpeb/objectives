# Objective 03 - Learn Kubernetes (Project Delivery)

## Expectation
Run performance tests using Kubernetes

## Measuring
* Harvest: Delivery Manager to track
* OneView: Two months after the period ends, we'll know if there has been any mismatches.

## Justification
Learn how to use the tool by:
* Create learning guide/investigation document
* Watch training videos/documentation
* Run performance tests using Kubernetes clusters

## Results

## Evidence

