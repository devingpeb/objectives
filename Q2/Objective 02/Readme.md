# Objective 02 - Learn NeoLoad (Project Delivery)

## Expectation
NeoLoad Migration

## Measuring
1. Complete training in NeoLoad
2. Complete videos in Neotys Academy
3. Create NeoLoad scripts:
* API Level
* Browser Level (+ Selenium)
* Mobile (+ Appium)

## Justification
* Client has acquired NeoLoad tool, and will be the tool of preference to do performance testing.
* Migration of the current scripts is in scope for the 2020 Roadmap

## Results

## Evidence