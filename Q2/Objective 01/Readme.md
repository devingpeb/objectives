# Objective 01 - Performance Testing in Cloud (Project Delivery)

## Expectation
* Learn about Google Cloud Platform
* Learn about Azure

## Measuring
Learn how to use the tool by:
* Create learning guide/investigation document
* Watch training videos/documentation 
* Run performance tests in Cloud

## Justification
Some applications are being migrated to Cloud (GCP and Azure) and we're being required to do performance executions using VMs inside the Cloud

## Results


## Evidence