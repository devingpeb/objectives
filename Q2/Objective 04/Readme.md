# Objective 04 - Data on Demand Demo (Practice)

## Expectation
Create a Data on Demand Demo for potential DOU Potential clients

## Measuring
* Build environment to run the demo for Data on Demand
* Run and record demo for Data on Demand

## Justification
Scope has been defined to prepare the demo for Data on Demand, and the IP Development team is working towards completing it so we can present it to the client

## Results

## Evidence