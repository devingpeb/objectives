# Objective 06 - Learn Appium (Personal)

## Expectation
Create Appium Scripts

## Measuring
Learn how to use the tool by:
* Create learning guide/investigation document
* Watch training videos/documentation
* Create sample Appium scripts

## Justification
People with multiskills are more likely to get assigned to projects. In addition, acquiring new skills would help me to gain more knowledge about the QE process and I can do better contributions to both the delivery and the practice

## Results

## Evidence