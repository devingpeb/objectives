## Objectives Digital On Us

Tracking of objectives for Q2 2020

| # | Type             | Name                        | Status      |
|:--|:-----------------|:----------------------------|:------------|
| 1 | Project Delivery | Performance Testing in Clod | In Progress |
| 2 | Project Delivery | Learn NeoLoad 	             | In Progress |
| 3 | Project Delivery | Learn Kubernetes            | In Progress |
| 4 | Practice         | Create Data on Demand Demo  | In Progress |
| 5 | Practice         | Conduct Interviews          | In Progress |
| 6 | Personal         | Learn Appium                | In Progress |

