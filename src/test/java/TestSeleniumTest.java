import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class TestSeleniumTest {

    @Test
    public void TestSeleniumTest () throws  MalformedURLException {

        DesiredCapabilities dr= DesiredCapabilities.chrome();

        RemoteWebDriver driver=new RemoteWebDriver(new URL("http://selenium__standalone-chrome:4444/wd/hub"), dr);

        try {
            driver.navigate().to("http://demo.guru99.com/");
            System.out.println("---------------Step 1 Success-------------");
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println("---------------Step 1 Failed-------------" + e.getMessage());
            driver.quit();
        }

        try {
            driver.findElement(By.xpath("//input[@name='emailid']")).sendKeys("abc@gmail.com");
            System.out.println("---------------Step 2 Success-------------");
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println("---------------Step 2 Failed-------------" + e.getMessage());
            driver.quit();
        }

        try {
            driver.findElement(By.xpath("//input[@name='btnLogin']")).click();
            System.out.println("---------------Step 3 Success-------------");
            Thread.sleep(3000);
        } catch (Exception e) {
            System.out.println("---------------Step 3 Failed-------------" + e.getMessage());
            driver.quit();
        }

        driver.quit();

    }
}
