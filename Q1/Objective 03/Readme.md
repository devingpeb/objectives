# Objective 03 - Harvest/OneView's correctness and on-time submission (Project Delivery)

## Expectation
Fill-in and submit harvest and OneView every Friday before 12:00 PM CST. Captured time must on the correct task and billable hours should match between those two. Absences, when applicable (PTOs, sick leaves, paternity/maternity leaves, compensations), should be supported by Zoho request previously approved by Delivery Manager, and backed up by Client's approval via email.

## Measuring
* Harvest: Delivery Manager to track
* OneView: Two months after the period ends, we'll know if there has been any mismatches.

## Justification
Right now, a considerable amount of effort is invested on reviewing time-sheets, and most of the times submitting them on behalf of the collaborators, increasing the risk of delayed invoices, therefore delaying client's payments. Mismatches between DOU's invoices and Client's time tracking will cause a delay on the invoice's payment, sometimes moving deltas to the following period

## Results
All timesheets have been submitted on time, and comments have been added.

## Evidence
Add screenshots
