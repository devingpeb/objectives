# Objective 05 - Conduct Interviews (Practice)

## Expectation
Contribute with DOU - QE Practice Team interviewing candidates for hiring

## Measuring
* Get Knowledge transfer from Omar
* Shadow an interview from Omar
* Do a reverse interview with Omar
* Conduct interviews

## Justification
DigitalOnUs is continuously looking for people to fill in roles and for that matter, interviewing candidates is a very important task. As we continue to grow as a company, more roles are open, therefore more people needs to be intereviewed and more interviewers are needed

## Results
Update performance testing interview template

## Evidence
Link to Merge Request:
https://gitlab.com/DigitalOnUs/quality-engineering/interviews-questionnaires/codinginterview/-/merge_requests/22

