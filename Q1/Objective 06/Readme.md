# Objective 06 - Learn Selenium (Personal)

## Expectation
Learn Selenium basics

## Measuring
1. Learn how to use the tool by:
* Create a research document
* Watching training videos
2. Create a sample Selenium script

## Justification
People with multiskills are more likely to get assigned to projects. In addition, acquiring new skills would help me to gain more knowledge about the QA process and I can do better contributions to both the delivery and the practice

## Results
Selenium sample script has been created.
Selenium sample script has been integrated into a CI/CD pipeline.

## Evidence
Add Gitlab URL