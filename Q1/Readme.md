## Objectives Digital On Us

Tracking of objectives for Q1 2020

| # | Type             | Name                                                   | Status       |
|:--|:-----------------|:-------------------------------------------------------|:-------------|
| 1 | Project Delivery | Learn Blue Triangle                                    | Add evidence |
| 2 | Project Delivery | Integrate Jmeter in Gitlab                             | Add evidence |
| 3 | Project Delivery | Harvest/OneView's correctness and on-time submission   | Add evidence |
| 4 | Practice         | Data on Demand Demo                                    | Complete     |
| 5 | Practice         | Conduct Interviews                                     | Complete     |
| 6 | Personal         | Learn Selenium                                         | Run pipeline |

