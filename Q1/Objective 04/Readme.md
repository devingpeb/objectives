# Objective 04 - Data on Demand Demo (Practice)

## Expectation
Create a Data on Demand Demo for potential DOU Potential clients

## Measuring
Coordinate with IP Development team to define the requirements for the demo for Data on Demand

## Justification
In order to support the practice, IP Development team is working on different projects, I will be taking care of the demo for Data on Demand

## Results
Scope has been defined to prepare the demo for Data on Demand, and the IP Development team is working towards completing it so we can present it to the client

## Evidence
https://trello.com/c/x0pAUmbh