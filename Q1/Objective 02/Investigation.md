# Integrate Jmeter with CI/CD Pipeline

## Pre-requisites
1. Jmeter installation file
2. Jmeter script and data files

## Steps to integrate Jmeter in the Gitlab CI/CD Pipeline

1. Create an stage in the gitlab-ci.yml file for the performance test
2. Create a job uses openjdk as an image, also copy the Jmeter.tar file to the instance created
3. Run the script using jmeter.sh file 
4.  