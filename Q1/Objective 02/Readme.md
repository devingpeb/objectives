# Objective 02 - Integrate Jmeter in Gitlab (Project Delivery)

## Expectation
Contribute doing research, coding and testing, as well as coordinating with project teams in order to get knowledge about their pipeline process and determine what would be the best approach to implement it

## Measuring
1. Learn about Pipelines by:
* Create investigation document
* Search for videos and KT documentation
2. Learn how to integrante GitLab and Jmeter by:
* Research about documentation
3. Create a sample test that can run a Jmeter script within a Gitlab Pipeline

## Justification
Client Performance Team wants to integrate Jmeter scripts to the POD teams’ pipelines

## Results
9 teams have been migrated to CI/CD Pipeline for Shift Left Approach, and 7 more teams are currently in progress.

## Evidence
Investigation Document:
https://gitlab.com/devingpeb/objectives/-/blob/master/Q1/Objective%2002/Investigation.md
Pipeline URL:
https://gitlab.com/devingpeb/objectives/-/pipelines/148923981
