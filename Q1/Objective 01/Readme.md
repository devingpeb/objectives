# Objective 01 - Learn Blue Triangle (Project Delivery)

## Expectation
Learn how to monitor and report with Blue Triangle tool

## Measuring
1. Learn how to use the tool by:
* Weekly trainings with Blue Triangle team
* Watching training videos
* Create investigation document
2. Follow up on reporting activities of the tool
* Publish weekly report
* Engage additional teams to include synthetic monitors

## Justification
* Performance team is doing Production Monitoring using Blue Triangle technology and keeps the involved teams from the Customer Technology Teams.
* DOU QE Practice can take advantage of the tool.

## Results
Investigation was carried out about the teams that were using the tool. Blue Triangle has been taken away from Kroger, Dynatrace and other open source tools will we the replacement.

## Evidence
Find the evidence under this URL
